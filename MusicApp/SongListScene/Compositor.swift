//
//  Compositor.swift
//  MusicApp
//
//  Created by Anton Hoang on 3/13/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation

struct Compositor {
    var groupName: String
    var songName: String
}
