//
//  CompositorViewCell.swift
//  MusicApp
//
//  Created by Anton Hoang on 3/12/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit

class CompositorViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: "CompositorViewCell")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configurate(compositor: Compositor) {
        textLabel?.text = compositor.songName
        detailTextLabel?.text = compositor.groupName
    }
}
