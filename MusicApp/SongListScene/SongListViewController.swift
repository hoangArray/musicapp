//
//  ViewController.swift
//  MusicApp
//
//  Created by Anton Hoang on 3/12/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit
import SnapKit
import AVFoundation
import Foundation

class SongListViewController: UIViewController {
    
    //MARK: - Properties
    private var tableView: UITableView!
    private var compositorList = [Compositor]()
    private var songsByURL = [URL]()
    private let networkService: Networkable!
    
    init(networkService: Networkable) {
        self.networkService = networkService
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupSongList()
    }
    
    private func setupSongList() {
        networkService.getSongsFromURL {
            _ = $0.files.map { [unowned self] file in
                let titles = self.findDash(name: file.name)
                guard let groupName = titles.first,
                    let songName = titles.last,
                    let url = URL(string: file.streamingLink) else { return }

                DispatchQueue.main.async { [unowned self] in
                    self.compositorList.append(Compositor(groupName: groupName, songName: songName))
                    self.songsByURL.append(url)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func findDash(name: String) -> [String] {
        var songName = name
        var findDash = [String]()
        if songName.contains("mp3") {
            songName = songName.replacingMultipleOccurrences(using: (of: ".mp3", with: ""))
            findDash = songName.components(separatedBy: [" - "])
        }
        return findDash
    }
    
    //MARK: - setupUI
    private func setupViews() {
        title = "Playlist"
        tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(CompositorViewCell.self, forCellReuseIdentifier: "CompositorViewCell")
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints( { make in
            make.top.equalTo(view.snp.top)
            make.leading.equalTo(view.snp.leading)
            make.trailing.equalTo(view.snp.trailing)
            make.bottom.equalTo(view.snp.bottom)
        })
    }
}

extension SongListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return compositorList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompositorViewCell", for: indexPath) as! CompositorViewCell
        let compositor = compositorList[indexPath.row]
        cell.configurate(compositor: compositor)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let playerViewController = PlayerViewController()
        playerViewController.songsByURL = songsByURL
        playerViewController.compositors = compositorList
        playerViewController.currentIndex = indexPath.row
        present(playerViewController, animated: true, completion: nil)
    }
}


