//
//  SongURL.swift
//  MusicApp
//
//  Created by Anton Hoang on 3/14/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation

struct SongURL: Codable {
    var files: [Files]
    
    enum CodingKeys: String, CodingKey {
        case files = "Files"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        files = try container.decode([Files].self, forKey: .files)
    }
}

struct Files: Codable {
    var name: String
    var streamingLink: String
    
    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case streamingLink = "StreamingLink"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        streamingLink = try container.decode(String.self, forKey: .streamingLink)
    }
}
