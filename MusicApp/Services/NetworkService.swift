//
//  NetworkService.swift
//  MusicApp
//
//  Created by Anton Hoang on 3/14/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation

protocol Networkable {
    func getSongsFromURL(completion: @escaping (SongURL) -> ())
}

class NetworkService: Networkable {
    
    func getSongsFromURL(completion: @escaping (SongURL) -> ()) {
        guard let url = URL(string: "https://dev.opendrive.com/api/v1/folder/shared.json/NjNfOTQ2Mjg3Xw?order_type=asc")
            else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                let result = try decoder.decode(SongURL.self, from: data)
                completion(result)
            } catch {
                print(error)
            }
        }.resume()
    }
}
