//
//  Extensions.swift
//  MusicApp
//
//  Created by Anton Hoang on 3/12/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import AVKit

public extension String {
    func components(separatedBy separators: [String]) -> [String] {
        var output: [String] = [self]
        separators.forEach { separator in
            output = output.flatMap  {
                $0.components(separatedBy: separator)
            }
        }
        return output.map { $0.trimmingCharacters(in: .whitespaces)}
    }
}

public extension String {
    func replacingMultipleOccurrences<T: StringProtocol, U: StringProtocol>(using array: (of: T, with: U)...) -> String {
        var str = self
        for (a, b) in array {
            str = str.replacingOccurrences(of: a, with: b)
        }
        return str
    }
}

extension CMTime {
    func displayTimeAsString() -> String {
        guard !CMTimeGetSeconds(self).isNaN else { return "" }
        let totalSeconds = Int(CMTimeGetSeconds(self))
        let seconds = totalSeconds % 60
        let minutes = totalSeconds / 60
        let timeFormater = String(format: "%02d:%02d", minutes, seconds)
        return timeFormater
    }
}
