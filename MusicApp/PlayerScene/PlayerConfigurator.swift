//
//  PlayerView.swift
//  MusicApp
//
//  Created by Anton Hoang on 3/13/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import Foundation
import UIKit


class PlayerConfigurator: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        
        backgroundColor = .clear
        addSubview(playPauseButton)
        addSubview(prevSongButton)
        addSubview(nextSongButton)
        addSubview(progressBarSlider)
        addSubview(startDurationLabel)
        addSubview(finishDurationLabel)
        addSubview(compositorImage)
        addSubview(compositorDetailView)
        
        compositorImage.snp.makeConstraints {
            $0.top.equalTo(snp.top).offset(40)
            $0.leading.equalTo(snp.leading)
            $0.trailing.equalTo(snp.trailing)
            $0.height.equalTo(snp.height).dividedBy(2.5)
            $0.centerX.equalTo(snp.centerX)
        }
        
        compositorDetailView.snp.makeConstraints {
            $0.top.equalTo(compositorImage.snp.bottom)
            $0.leading.equalTo(compositorImage.snp.leading)
            $0.trailing.equalTo(compositorImage.snp.trailing)
            $0.height.equalTo(80)
        }
        
        progressBarSlider.snp.makeConstraints {
            $0.top.equalTo(compositorDetailView.snp.bottom).offset(10)
            $0.leading.equalTo(compositorDetailView.snp.leading)
            $0.trailing.equalTo(compositorDetailView.snp.trailing)
        }
        
        startDurationLabel.snp.makeConstraints {
            $0.top.equalTo(progressBarSlider.snp.bottom)
            $0.leading.equalTo(progressBarSlider.snp.leading)
        }
        
        finishDurationLabel.snp.makeConstraints {
            $0.top.equalTo(startDurationLabel.snp.top)
            $0.trailing.equalTo(progressBarSlider.snp.trailing)
        }
        
        prevSongButton.snp.makeConstraints {
            $0.top.equalTo(startDurationLabel.snp.bottom).offset(20)
            $0.height.equalTo(30)
            $0.width.equalTo(snp.width).dividedBy(3)
            $0.leading.equalTo(compositorImage.snp.leading)
            $0.trailing.equalTo(playPauseButton.snp.leading)
        }
        
        playPauseButton.snp.makeConstraints {
            $0.centerX.equalTo(snp.centerX)
            $0.trailing.equalTo(nextSongButton.snp.leading)
            $0.top.equalTo(prevSongButton.snp.top)
            $0.width.equalTo(prevSongButton.snp.width)
            $0.bottom.equalTo(prevSongButton.snp.bottom)
        }
        
        nextSongButton.snp.makeConstraints {
            $0.top.equalTo(playPauseButton.snp.top)
            $0.trailing.equalTo(snp.trailing)
            $0.width.equalTo(playPauseButton.snp.width)
            $0.bottom.equalTo(playPauseButton.snp.bottom)
        }
    }
    
    lazy var startDurationLabel: UILabel = {
        let start = UILabel()
        start.font = UIFont.boldSystemFont(ofSize: 15)
        start.textColor = UIColor(red: 209/255, green: 209/255, blue: 214/255, alpha: 1)
        start.text = "0:00"
        return start
    }()
    
    lazy var finishDurationLabel: UILabel = {
        let finish = UILabel()
        finish.font = UIFont.boldSystemFont(ofSize: 15)
        finish.textColor = UIColor(red: 209/255, green: 209/255, blue: 214/255, alpha: 1)
        finish.text = "--:--"
        return finish
    }()
    
    lazy var compositorGroupName: UILabel = {
        let groupName = UILabel()
        groupName.text = "Artist"
        groupName.textColor = .systemRed
        return groupName
    }()
    
    lazy var compositorSongName: UILabel = {
        let songName = UILabel()
        songName.font = UIFont.boldSystemFont(ofSize: 17)
        songName.text = "Whisper lyrics feat sommeone"
        return songName
    }()
    
    lazy var compositorDetailView: UIView = {
        let compositorView = UIView()
        compositorView.backgroundColor = .clear
        compositorView.addSubview(compositorGroupName)
        compositorView.addSubview(compositorSongName)
        
        compositorSongName.snp.makeConstraints {
            $0.top.equalTo(compositorView.snp.top).offset(20)
            $0.leading.equalTo(compositorView.snp.leading)
            $0.trailing.equalTo(compositorView.snp.trailing)
        }
        
        compositorGroupName.snp.makeConstraints {
            $0.top.equalTo(compositorSongName.snp.bottom).offset(5)
            $0.leading.equalTo(compositorSongName.snp.leading)
            $0.trailing.equalTo(compositorSongName.snp.trailing)
        }
        
        return compositorView
    }()
    
    lazy var playPauseButton: UIButton = {
        let playPause = UIButton(type: .custom)
        playPause.backgroundColor = .clear
        playPause.imageView?.contentMode = .scaleAspectFit
        playPause.setImage(UIImage(named: "pause"), for: .normal)
        return playPause
    }()
    
    lazy var prevSongButton: UIButton = {
        let prevSong = UIButton(type: .custom)
        prevSong.setImage(UIImage(named: "rewind"), for: .normal)
        prevSong.imageView?.contentMode = .scaleAspectFit
        prevSong.backgroundColor = .clear
        return prevSong
    }()
    
    lazy var nextSongButton: UIButton = {
        let nextSong = UIButton(type: .custom)
        nextSong.setImage(UIImage(named: "fast-forward"), for: .normal)
        nextSong.imageView?.contentMode = .scaleAspectFit
        nextSong.backgroundColor = .clear
        return nextSong
    }()
    
    lazy var progressBarSlider: UISlider = {
        let progressBar = UISlider()
        progressBar.thumbTintColor = .gray
        progressBar.minimumTrackTintColor = .gray
        //change this prop for updating value change
        //        progressBar.isContinuous = false
        return progressBar
    }()
    
    lazy var compositorImage: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.image = UIImage(named: "melody")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
}
