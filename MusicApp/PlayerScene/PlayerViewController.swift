//
//  PlayerViewController.swift
//  MusicApp
//
//  Created by Anton Hoang on 3/12/20.
//  Copyright © 2020 Anton Hoang. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import SnapKit

enum SongMode {
    case play
    case pause
    
    mutating func toogle() {
        switch self {
        case .play: self = .pause
        case .pause: self = .play
        }
    }
}

class PlayerViewController: UIViewController {

    //MARK: - Properties
    var playerConfigurator = PlayerConfigurator()
    private var songMode: SongMode = .pause
    private var playerItem: AVPlayerItem!
    var currentIndex: Int!
    var songsByURL: [URL]!
    var compositors: [Compositor]!
    let throttler = Throttler(minimumDelay: 1, queue: DispatchQueue.global(qos: .userInteractive))
    
    private lazy var player: AVPlayer = {
        let player = AVPlayer()
        return player
    }()
        
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialSettings()
        setupViews()
        setupActions()
        observePlayerCurrentTime()
    }

    //MARK: - setupUI
    private func setupInitialSettings() {
        tooglePlayPauseImage(imageString: "pause")
        setupPlayer(url: songsByURL[currentIndex])
        setupCompositor(compositor: compositors[currentIndex])
    }
    
    private func setupViews() {
        view.backgroundColor = .white
        view.addSubview(playerConfigurator)
        
        playerConfigurator.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide.snp.topMargin)
            $0.leading.equalTo(view.snp.leading).offset(20)
            $0.trailing.equalTo(view.snp.trailing).offset(-20)
            $0.bottom.equalTo(view.snp.bottom)
            $0.centerX.equalTo(view.snp.centerX)
        }
    }
    
    private func observePlayerCurrentTime() {
        let time = CMTimeMake(value: 1, timescale: 1)
        player.addPeriodicTimeObserver(forInterval: time, queue: .main) { [weak self] in
            
            guard let duration = self?.player.currentItem?.duration else { return }
            let currentDurationTime = (duration - $0).displayTimeAsString()
            
            self?.playerConfigurator.startDurationLabel.text = $0.displayTimeAsString()
            self?.playerConfigurator.finishDurationLabel.text = "-\(currentDurationTime)"
            self?.sliderDuration()
        }
    }
    
    private func sliderDuration() {
        let currentSeconds = CMTimeGetSeconds(player.currentTime())
        guard let time = player.currentItem?.duration else { return }
        let duration = CMTimeGetSeconds(time)
        let percent = currentSeconds / duration
        playerConfigurator.progressBarSlider.value = Float(percent)
    }
    
    //MARK: - setup player
    private func setupPlayer(url: URL) {
        songMode = .play
        songMode.toogle()
        tooglePlayPauseImage(imageString: "pause")
        playerItem = AVPlayerItem(url: url)
        player.replaceCurrentItem(with: playerItem)
        player.automaticallyWaitsToMinimizeStalling = false
        player.playImmediately(atRate: 1.0)
    }
    
    //MARK: - setup player actions
    private func setupActions() {
        playerConfigurator.playPauseButton.addTarget(self, action: #selector(playPause), for: .touchUpInside)
        playerConfigurator.prevSongButton.addTarget(self, action: #selector(prevSongPlay), for: .touchUpInside)
        playerConfigurator.nextSongButton.addTarget(self, action: #selector(nextSongPlay), for: .touchUpInside)
        playerConfigurator.progressBarSlider.addTarget(self, action: #selector(rewindingSlider(sender:)), for: .valueChanged)
    }
    
    @objc private func rewindingSlider(sender: UISlider) {
        let percentage = sender.value
        throttler.throttle { [unowned self] in
            guard let duration = self.player.currentItem?.duration else { return }
            let durationSeconds = CMTimeGetSeconds(duration)
            let seekTime = Float64(percentage) * durationSeconds
            let sk = CMTimeMakeWithSeconds(seekTime, preferredTimescale: 1)
            self.player.seek(to: sk)
        }
    }
    
    @objc private func playPause() {
        songMode.toogle()
        switch songMode {
        case .play:
            tooglePlayPauseImage(imageString: "play")
            player.pause()
        case .pause:
            tooglePlayPauseImage(imageString: "pause")
            player.play()
        }
    }
    
    private func tooglePlayPauseImage(imageString: String) {
        let image = UIImage(named: imageString)
        playerConfigurator.playPauseButton.setImage(image, for: .normal)
    }
    
    @objc private func prevSongPlay() {
        guard let firstSong = songsByURL.first else { return }
        if songsByURL[currentIndex] == firstSong {
            return
        }
        setupPlayer(url: songsByURL[currentIndex - 1])
        let compositor = compositors[currentIndex - 1]
        setupCompositor(compositor: compositor)
        currentIndex -= 1
    }
    
    @objc private func nextSongPlay() {
        guard let lastSong = songsByURL.last else { return }
        if songsByURL[currentIndex] == lastSong {
            return
        }
        setupPlayer(url: songsByURL[currentIndex + 1])
        let compositor = compositors[currentIndex + 1]
        setupCompositor(compositor: compositor)
        currentIndex += 1
    }
    
    private func setupCompositor(compositor: Compositor) {
        playerConfigurator.compositorSongName.text = compositor.songName
        playerConfigurator.compositorGroupName.text = compositor.groupName
        
        playerConfigurator.progressBarSlider.value = 0
        playerConfigurator.startDurationLabel.text = "0:00"
        playerConfigurator.finishDurationLabel.text = "--:--"
    }
}

class Throttler {

    private var workItem: DispatchWorkItem = DispatchWorkItem(block: {})
    private var previousRun: Date = Date.distantPast
    private let queue: DispatchQueue
    private let minimumDelay: TimeInterval

    init(minimumDelay: TimeInterval, queue: DispatchQueue = DispatchQueue.main) {
        self.minimumDelay = minimumDelay
        self.queue = queue
    }

    func throttle(_ block: @escaping () -> Void) {
        // Cancel any existing work item if it has not yet executed
        workItem.cancel()

        // Re-assign workItem with the new block task, resetting the previousRun time when it executes
        workItem = DispatchWorkItem() {
            [weak self] in
            self?.previousRun = Date()
            block()
        }

        // If the time since the previous run is more than the required minimum delay
        // => execute the workItem immediately
        // else
        // => delay the workItem execution by the minimum delay time
        let delay = previousRun.timeIntervalSinceNow > minimumDelay ? 0 : minimumDelay
        queue.asyncAfter(deadline: .now() + Double(delay), execute: workItem)
    }
}
